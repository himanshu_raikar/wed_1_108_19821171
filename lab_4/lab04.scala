// CSE3BDC/CSE5BDC Lab 04 - Programming in Scala
/*******************************************************************************
 * Exercise 1
 * Declare a variable, initially setting it's value to your name
 * (as a String). Now try changing the same variable to your age (as an Int).
 * What happens and why?
 */
// TODO: Write your code here
var Name = "Himanshu"
Name = 30
print(Name)
// TODO: Write the reason for the above behavior here

//Answer - Type Mismatch error : We are trying to change the the variable value to 'integer' when it is set to accept 'string'



/*******************************************************************************
 * Exercise 2
 * Use the zip method to write code which calculates the dot product
 * of two vectors.
 */
val u = List(2, 5, 3)
val v = List(4, 3, 7)
val dot = u.zip(v).map(x => x._1 * x._2).sum
print(dot)


//Answer 44


/*******************************************************************************
 * Exercise 3
 * Write a function that returns the character at index n of a given word if
 * the word is long enough - otherwise return '-'.
 */
def extract(word: String, n: Int) : Char = {
  if(n<word.length()) 
    { word(n);
    }
    else
    {'-';
    }
}

val names = List("Peter", "John", "Mary", "Henry")
val result = names.map(extract(_,4))
print(result)


result: List[Char] = List(r, -, -, y)

/*******************************************************************************
 * Exercise 4
 * Write a function which prints the maximum value in a sequence.
 */
def find_max(sequence: Seq[Int]) : Int = {
    sequence.reduce(Math.max)
}

val numberList = List(4, 7, 2, 1)
find_max(numberList)

res15: Int = 7

val numberArray = Array(4, 7, 2, 1)
find_max(numberArray)


res16: Int = 7


/*******************************************************************************
 * Exercise 5
 * Write a function which, when given two integer sequences, returns
 * a List of the numbers which appear at the same position in both sequences.
 */
def matchedNumbers(a: Seq[Int], b: Seq[Int]) : Seq[Int] = {
   a.zip(b).filter(x => x._1 == x._2).map(x => x._1)
}
val list1 = List(1,2,3,10)
val list2 = List(3,2,1,10)
val mn = matchedNumbers(list1, list2)



mn: Seq[Int] = List(2, 10)


/*******************************************************************************
 * Exercise 6
 * Write a function which, when given two integer sequences, returns
 * a List of the numbers which appear at the same position in both sequences.
 */

def eligibility(person: (String, Int, String)) : Boolean = {
          person match {
              case (name, age:Int, gender:String)
              if (age<13 & gender == "male") => true
              case (name, age:Int, gender:String)
              if (age>13 & gender == "male") => false ==
              println (name +" is too old")
              case (name, age:Int, gender:String)
              if (age<13 & gender == "female") => false ==
              println (name +" is not male")
              case (name, age:Int, gender:String)
              if (age>13 & gender == "female") => false ==
              println (name +" is too old and not male")
          }
}

val people = List(("Harry", 15, "male"), ("Peter", 10, "male"),
                  ("Michele", 20, "female"), ("Bruce", 12, "male"),
                  ("Mary", 2, "female"), ("Max", 22, "male"))
val allowedEntry = people.filter(eligibility(_))
// TODO: Copy and paste the result here

Harry is too old
Michele is too old and not male
Mary is not male
Max is too old
allowedEntry: List[(String, Int, String)] = List((Peter,10,male), (Bruce,12,male))

print(allowedEntry)
// TODO: Copy and paste the result here

List((Peter,10,male), (Bruce,12,male))



/*
 * SCRATCHPAD
 
 val numbers = Array(2,13,7,22)
 numbers(2) = 10
 for(i<-numbers)println(i)
 
  //////////////////////////////////////////////////////////////////////
  
 var letters = List("A","B","C","D")
 letters = "E" :: letters
 val animals = List ("Cat","Dog","Shark","Elephant")
 letters = letters ::: animals
 
  //////////////////////////////////////////////////////////////////////
  
 def greet(name:String){
 println("Hello "+name+"!")
 }
 val names = List("Peter","John","Mary","Henry")
 names.foreach(greet(_))
 
  //////////////////////////////////////////////////////////////////////
  
 def multiply(x:Int,y:Int):Int={
 x*y
 }
 val numbers = 1 to 10
 val largeNumbers = numbers.map(multiply(_,3))
 
 //////////////////////////////////////////////////////////////////////
 
 def extract (word:String,n:Int) : Unit = {
 if(n<word.length()) 
 {(word(n));
 }
 else {
 ('-');
 }}
 
 val names = List("Peter", "John", "Mary", "Henry")
 val result = names.map(extract(_,4))
 print(result)
  //////////////////////////////////////////////////////////////////////
  
  val x = 2
  x match {
   case 1 => print("one")
  | case 2 => print("two")
  | case 3 => print("three")
  }
  
  //////////////////////////////////////////////////////////////////////
  
  val thing:Any = ("Jim", 21)
  thing match {
   case (name, age:Int) if age < 18 => print(name + " is young")
   case (name, 18) => print(name+ " is eighteen")
   case (name, age) => print(name + " is an adult")
   case _ => print("This isn't a person!")
  }
  
  //////////////////////////////////////////////////////////////////////
  
  val numbers = List(1,4,3,7,5,9)
  val mapped = numbers.map(_ match {
   case 5 => 0
   case x if x < 5 => -1
   case x if x > 5 => 1
   })
 print(mapped)
  
  //////////////////////////////////////////////////////////////////////
  
  def find_max (nummax : Seq[Int]) : 
  
  
  
 * Play around, and save any helpful Scala commands below this section
 */

