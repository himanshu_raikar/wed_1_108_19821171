// CSE3BDC/CSE5BDC Lab 05 - Processing Big Data with Spark
/*******************************************************************************
 * Exercise 1
 * Write code that first finds the square root of each number in an RDD and then
 * sums all the square roots together.
 */
// TODO: Write your code here

val someNumbers = sc.parallelize(1 to 1000)
val result = someNumbers.map(math.sqrt(_)).sum

// TODO: Copy and paste the result here

result: Double = 21097.45588748074


/*******************************************************************************
 * Exercise 2
 * Sum up the total salary for each occupation and then report the output in
 * ascending order according to occupation - in *one line* of code.
 */
val people = sc.parallelize(Array(("Jane", "student", 1000),
                                  ("Peter", "doctor", 100000),
                                  ("Mary", "doctor", 200000),
                                  ("Michael", "student", 1000)))
val result = people.map {case (name, occupation, salary) => ((occupation),(salary))}.reduceByKey(_+_).sortBy(_._1, true)
result.collect

// TODO: Copy and paste the result here

res0: Array[(String, Int)] = Array((doctor,300000), (student,2000))


/*******************************************************************************
 * Exercise 3
 * Use the map method to create a new pair RDD where the key is "native-country"
 * and the value is age. Use the filter function to remove records with missing
 * data.
 */
// TODO: Write your code here
val countryAge = censusSplit.map(r => (r(13),r(0).toInt)).filter(r=>r._1 != "?") 
                                 
println(countryAge.count()) 

31978  // Does this equal 31978? - YES
println(countryAge)

MapPartitionsRDD[20] at filter at <console>:28 //COULD NOT PRINT REQUIRED RESULT

scala> countryAge.top(100) //Used this command o print result

res15: Array[(String, Int)] = Array((Yugoslavia,66), (Yugoslavia,56), (Yugosla
via,56), (Yugoslavia,45), (Yugoslavia,43), (Yugoslavia,41), (Yugoslavia,41), (
Yugoslavia,40), (Yugoslavia,36), (Yugoslavia,35), (Yugoslavia,35), (Yugoslavia
,31), (Yugoslavia,29), (Yugoslavia,25), (Yugoslavia,22), (Yugoslavia,20), (Vie
tnam,73), (Vietnam,73), (Vietnam,70), (Vietnam,63), (Vietnam,54), (Vietnam,53)
, (Vietnam,52), (Vietnam,51), (Vietnam,50), (Vietnam,48), (Vietnam,46), (Vietn
am,45), (Vietnam,45), (Vietnam,45), (Vietnam,44), (Vietnam,44), (Vietnam,44), 
(Vietnam,43), (Vietnam,43), (Vietnam,41), (Vietnam,40), (Vietnam,38), (Vietnam
,37), (Vietnam,37), (Vietnam,36), (Vietnam,35), (Vietnam,35), (Vietnam,33), (V
ietnam,31), (Vietnam,31), (Vietnam,30), (Vietnam,30), (Vietnam,30), (Vietnam,3
0), (Vietnam,30),...





/*******************************************************************************
 * Exercise 4
 * Find the age of the oldest person from each country so the resulting RDD
 * should contain one (country, age) pair per country. 
 */
// TODO: Write your code here
val oldestPerCountry = countryAge.distinct().reduceByKey(math.max(_,_))

println(oldestPerCountry)
ShuffledRDD[22] at reduceByKey at <console>:30 //UNABLE TO PRINT REQUIRED RESULT

scala> oldestPerCountry.top(50)
// TODO: Copy and paste the result here

res13: Array[(String, Int)] = Array((Yugoslavia,66), (Vietnam,73), (United-Sta
tes,90), (Trinadad&Tobago,61), (Thailand,55), (Taiwan,61), (South,90), (Scotla
nd,62), (Puerto-Rico,90), (Portugal,78), (Poland,85), (Philippines,90), (Peru,
69), (Outlying-US(Guam-USVI-etc),63), (Nicaragua,67), (Mexico,81), (Laos,56), 
(Japan,61), (Jamaica,66), (Italy,77), (Ireland,68), (Iran,63), (India,61), (Hu
ngary,81), (Hong,60), (Honduras,58), (Holand-Netherlands,32), (Haiti,63), (Gua
temala,66), (Greece,65), (Germany,74), (France,64), (England,90), (El-Salvador
,79), (Ecuador,90), (Dominican-Republic,78), (Cuba,82), (Columbia,75), (China,
75), (Canada,80), (Cambodia,65))



/*******************************************************************************
 * Exercise 5
 * Output the top 7 countries in terms of having the oldest person. The output
 * should again be the country followed by the age of the oldest person.
 */
// TODO: Write your code here
val top7OldestPerCountry = oldestPerCountry.sortBy(r=>r._2, false).take(7)

// TODO: Copy and paste the result here

top7OldestPerCountry: Array[(String, Int)] = Array((United-States,90), (South,
90), (Ecuador,90), (Puerto-Rico,90), (Philippines,90), (England,90), (Poland,8
5))


/*******************************************************************************
 * Exercise 6
 * Output the top 7 countries in terms of having the oldest person. The output
 * should again be the country followed by the age of the oldest person.
 */
// TODO: Write your code here
//STEP 1
val allPeople = censusSplit.map(r => (r(13), r(3), r(6), r(9)))
                                    
allPeople.take(5).foreach(println)
// TODO: Copy and paste the result here
(United-States,Bachelors,Adm-clerical,Male)
(United-States,Bachelors,Exec-managerial,Male)
(United-States,HS-grad,Handlers-cleaners,Male)
(United-States,11th,Handlers-cleaners,Male)
(Cuba,Bachelors,Prof-specialty,Female)

//STEP 2
val filteredPeople = allPeople.filter(r=>r._3 != "?") 
filteredPeople.count // Does this equal 30718? - YES
// TODO: Copy and paste the result here
res10: Long = 30718
                                    
//STEP 3
val canadians = filteredPeople.filter(r=>r._1 == "Canada")
val americans = filteredPeople.filter(r=>r._1 == "United-States")
canadians.count // Does this equal 107?
res11: Long = 107
americans.count // Does this equal 27504?
res12: Long = 27504

//STEP 4
                               
val repCandidates = canadians.map {case(a,b,c,d) => ((c),(a,b,d))}.join(americans.map {case(a,b,c,d) => ((c),(a,b,d))}).values

repCandidates.count // Does this equal 325711? - YES
// TODO: Copy and paste the result here
res3: Long = 325711
                                    
val includingDoctorate = repCandidates.filter(r=> r._1._2 == "Doctorate" | r._2._2 == "Doctorate")
includingDoctorate.count // Does this equal 31110? - YES
// TODO: Copy and paste the result here
res12: Long = 31110


/*
 * SCRATCHPAD
 * Play around, and save any helpful Scala/Spark commands below this section
 
val animalAges = sc.parallelize(List(("cat", 7), ("dog", 5), ("monkey", 3), ("cat", 6), ("dog", 10), ("bird", 1), ("bird", 1)))
val result = animalAges.reduceByKey(_+_)
result.collect

val result = animalAges.groupByKey()
result.collect

val animalLegs = sc.parallelize(List(("monkey", 2), ("dog", 4), ("cat", 4), ("bird", 2)))
val result = animalAges.join(animalLegs)
result.collect

val sortedAnimals = animalAges.sortBy(_._2, true)
sortedAnimals.collect

hdfs dfs -cat /user/ashhall1616/bdc_data/lab_5/census.txt | head

val censusLines = sc.textFile("/user/ashhall1616/bdc_data/lab_5/census.txt")
censusLines.take(5)
val censusSplit = censusLines.map(_.split(", "))

val countryOccupation = censusSplit.map(r=> (r(13),r(6)))
countryOccupation.saveAsTextFile("lab_5/countryOccupation")
countryOccupation.saveAsObjectFile("lab_5/countryOccupationBin")

val loadedCountryOccupation = sc.objectFile[(String,String)]("lab_5/countryOccupationBin")

loadedCountryOccupation.saveAsTextFile("file:///home/user198211713102/lab_5_countryOccupationText")

*/



