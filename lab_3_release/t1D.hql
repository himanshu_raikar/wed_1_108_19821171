set hivevar:studentId=19821171; --Please replace it with your student id 

DROP TABLE ${studentId}_mytraffic;
DROP TABLE ${studentId}_mydomains;
DROP TABLE ${studentId}_categorytraffic;

CREATE TABLE ${studentId}_mytraffic (URL STRING, ipAddress STRING, time TIMESTAMP)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INPATH './Input_data/1/traffic.csv'
INTO TABLE ${studentId}_mytraffic;

CREATE TABLE ${studentId}_mydomains (url STRING, category STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INPATH './Input_data/1/domains.csv'
INTO TABLE ${studentId}_mydomains;


CREATE TABLE ${studentId}_categorytraffic AS
SELECT ${studentId}_mydomains.category, COUNT (*) AS ct
FROM ${studentId}_mydomains INNER JOIN ${studentId}_mytraffic
ON (${studentId}_mydomains.URL = ${studentId}_mytraffic.URL)
GROUP BY ${studentId}_mydomains.category ORDER BY ct DESC LIMIT 5;

-- Dump the output to file
INSERT OVERWRITE LOCAL DIRECTORY './task1d-out/'
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
  STORED AS TEXTFILE
  SELECT * FROM ${studentId}_categorytraffic;

