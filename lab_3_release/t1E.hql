set hivevar:studentId=19821171; --Please replace it with your student id 

-- Don't forget to drop the tables you create here
DROP TABLE ${studentId}_ipcountries;
DROP TABLE ${studentId}_facebooktrafficcountries;
DROP TABLE ${studentId}_facebookregioncounts;


-- Task 1E step 2
-- Create a table with two columns - a list of ip addresses with
-- their associated region names.
CREATE TABLE ${studentId}_ipcountries AS
SELECT ${studentId}_myips.ipAddress AS IA, ${studentId}_myregions.regionName AS RN
FROM ${studentId}_myips CROSS JOIN ${studentId}_myregions 
WHERE ${studentId}_myips.intAddress <= ${studentId}_myregions.intMax AND ${studentId}_myips.intAddress >= ${studentId}_myregions.intMin; -- ...the ip address is between the IP lower and upper bounds

-- Task 1E step 3
-- Create a table with two columns - the name of a region for each hit to Facebook
-- between the two given dates, and the time of that visit.

CREATE TABLE ${studentId}_facebooktrafficcountries AS
SELECT ${studentId}_ipcountries.RN AS RegName, ${studentId}_facebooktraffic.time AS TrafficTime
FROM ${studentId}_ipcountries  INNER JOIN ${studentId}_facebooktraffic
ON ${studentId}_ipcountries.IA = ${studentId}_facebooktraffic.ipAddress;

-- Task 1E step 4
-- Create a table which contains the number of visits to Facebook from 
-- each country between the given dates.

CREATE TABLE ${studentId}_facebookregioncounts AS
SELECT RegName, COUNT (1) AS CT FROM ${studentId}_facebooktrafficcountries
GROUP BY RegName ORDER BY CT DESC;


-- Task 1E step 5
-- Write the contents of the table created in step 4 to the directory './task1e-out/'

INSERT OVERWRITE LOCAL DIRECTORY './task1e-out/'
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
  STORED AS TEXTFILE
  SELECT * FROM ${studentId}_facebookregioncounts;
