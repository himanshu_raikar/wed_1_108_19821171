set hivevar:studentId=19821171; --Please replace it with your student id 

DROP TABLE ${studentId}_mytraffic;

CREATE TABLE ${studentId}_mytraffic (URL STRING, ipAddress STRING, time TIMESTAMP)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INPATH './Input_data/1/traffic.csv'
INTO TABLE ${studentId}_mytraffic;


-- Dump the output to file
INSERT OVERWRITE LOCAL DIRECTORY './task1b-out/'
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
  STORED AS TEXTFILE
  SELECT URL, count(1) AS count
  FROM ${studentId}_mytraffic
  WHERE URL NOT LIKE ""
  GROUP BY URL ORDER BY count DESC;

