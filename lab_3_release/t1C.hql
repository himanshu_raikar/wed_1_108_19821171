set hivevar:studentId=19821171; --Please replace it with your student id 

DROP TABLE ${studentId}_mytraffic;
DROP TABLE ${studentId}_facebooktraffic;

CREATE TABLE ${studentId}_mytraffic (URL STRING, ipAddress STRING, time TIMESTAMP)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INPATH './Input_data/1/traffic.csv'
INTO TABLE ${studentId}_mytraffic;

CREATE TABLE ${studentId}_facebooktraffic AS
SELECT URL, ipAddress, time FROM ${studentId}_mytraffic
WHERE URL LIKE "www.Facebook.com" AND time > unix_timestamp('2014-02-14 00:00:00') AND time < unix_timestamp('2014-02-15 00:00:00');

-- Dump the output to file
INSERT OVERWRITE LOCAL DIRECTORY './task1c-out/'
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
  STORED AS TEXTFILE
  SELECT * FROM ${studentId}_facebooktraffic;

