import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


// Task 4b:
// TODO: *** Put your solution here ***
val publisherParquetDF = spark.read.parquet("file:///home/user198211713102/t4_story_publishers.parquet")

val df1 = publisherParquetDF.toDF("storyId1","publisher1")
val df2 = publisherParquetDF.toDF("storyId2","publisher2")

val dfJoined = df1.join(df2,$"storyId1" === $"storyId2" and $"publisher1" =!= $"publisher2")

val copublish = dfJoined.groupBy(greatest($"publisher1",$"publisher2") as ("Publisher1"), least($"publisher1",$"publisher2") as ("Publisher2")).agg(countDistinct($"storyId1") as "Count").sort(desc("Count"))

copublish.take(20).foreach(println)

copublish.write.mode("overwrite").csv("file:////home/user198211713102/t4_paired_publishers.csv")

// Required to exit the spark-shell
sys.exit(0)
