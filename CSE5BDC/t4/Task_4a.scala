import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


// Define case classe for input data
case class Article(articleId: Int, title: String, url: String, publisher: String,
                   category: String, storyId: String, hostname: String, timestamp: String)
// Read the input data
val articles = spark.read.
  schema(Encoders.product[Article].schema).
  option("delimiter", ",").
  csv("hdfs:///user/ashhall1616/bdc_data/assignment/t4/news.csv").
  as[Article]


// Task 4a:
// Step 1
// TODO: *** Put your solution here ***
val publisher = articles.select($"storyId",$"publisher").dropDuplicates("storyId","publisher")
publisher.take(10).foreach(println)

publisher.write.mode("overwrite").parquet("file:///home/user198211713102/t4_story_publishers.parquet")
val publisherParquetDF = spark.read.parquet("file:///home/user198211713102/t4_story_publishers.parquet")

// Step 2
// TODO: *** Put your solution here ***

val articleCount = publisherParquetDF.select($"storyId").groupBy($"storyId").agg(count("storyId") as "Count")
val popular = articleCount.where($"Count">4).sort(desc("Count"))
popular.take(10).foreach(println)


// Required to exit the spark-shell
sys.exit(0)
