import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


val docIds = scala.io.StdIn.readLine("Document IDs: ").split(" ")


// Task 3d:
// TODO: *** Put your solution here ***

val wordFirstParquetDF = spark.read.parquet("file:///home/user198211713102/t3_docword_index_part.parquet")
wordFirstParquetDF.cache.count()

for(docId <- docIds) {
    
    val check = wordFirstParquetDF.filter(($"docId").isin(docId)) //Filtered data to optimise data shuffle
    val checkFiltered = check.groupBy("docId").agg(max("count") as "MaxCount")
    val result = checkFiltered.join(wordFirstParquetDF,"docId").select($"docId",$"word",$"count").where($"MaxCount" === $"count").collect()
    result.take(1).foreach(println) 

}


// Required to exit the spark-shell
sys.exit(0)
