import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


// Define case classes for input data
case class Docword(docId: Int, vocabId: Int, count: Int)
case class VocabWord(vocabId: Int, word: String)

// Read the input data
val docwords = spark.read.
  schema(Encoders.product[Docword].schema).
  option("delimiter", " ").
  csv("hdfs:///user/ashhall1616/bdc_data/assignment/t3/docword.txt").
  as[Docword]
val vocab = spark.read.
  schema(Encoders.product[VocabWord].schema).
  option("delimiter", " ").
  csv("hdfs:///user/ashhall1616/bdc_data/assignment/t3/vocab.txt").
  as[VocabWord]


// Task 3a:
// TODO: *** Put your solution here ***
val SumCount = docwords.groupBy($"vocabId").agg(sum($"count").as("Total Count")).orderBy($"Total Count".desc)
val WordCount = SumCount.join(vocab,"vocabId").select($"word".as("Word"),$"Total Count")
WordCount.show(10)
WordCount.orderBy("Word").write.mode("overwrite").csv("file:////home/user198211713102/Task_3a-out/")


// Required to exit the spark-shell
sys.exit(0)
