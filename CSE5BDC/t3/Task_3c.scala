import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


val queryWords = scala.io.StdIn.readLine("Query words: ").split(" ")


// Task 3c:
// TODO: *** Put your solution here ***

val wordFirstParquetDF = spark.read.parquet("file:///home/user198211713102/t3_docword_index_part.parquet")
wordFirstParquetDF.cache.count()

for(queryWord <- queryWords) {
    
    val check = wordFirstParquetDF.filter(($"word").isin(queryWord)) //Filtered data to optimise data shuffle
    check.cache.count()
    val checkFiltered = check.groupBy("word").agg(max("count") as "MaxCount")
    val result = checkFiltered.join(wordFirstParquetDF,"word").select($"word",$"docId").where($"MaxCount" === $"count").collect()
    result.take(1).foreach(println)  
}

// Required to exit the spark-shell
sys.exit(0)
