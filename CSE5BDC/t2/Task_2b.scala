// Load the input data and split each line into an array of strings
val twitterLines = sc.textFile("hdfs:///user/ashhall1616/bdc_data/assignment/t2/twitter.tsv")
val twitterdata = twitterLines.map(_.split("\t"))

// Task 2b:
// TODO: *** Put your solution here ***

val result = twitterdata.map(r=> (r(1).substring(0,4),r(2).toInt)).groupByKey().map(r => (r._1,r._2.sum)).sortBy(r=>r._2, false)

result.take(1).foreach(println)


// Required to exit the spark-shell
sys.exit(0)
