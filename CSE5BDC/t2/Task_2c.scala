// Load the input data and split each line into an array of strings
val twitterLines = sc.textFile("hdfs:///user/ashhall1616/bdc_data/assignment/t2/twitter.tsv")
val twitterdata = twitterLines.map(_.split("\t"))

// Task 2c:
// TODO: *** Put your solution here ***

// Remember that each month is a string formatted as YYYYMM
val x = scala.io.StdIn.readLine("x month: ") 
val y = scala.io.StdIn.readLine("y month: ") 


val TweetIncrease = twitterdata.filter(r=>r(1)==x).map(r=>(r(3),r(2).toInt)).
            join(twitterdata.filter(r=>r(1)==y).map(r=>(r(3),r(2).toInt))).
            map(r=> (r._1,r._2,r._2._2-r._2._1)).sortBy(r=>r._3, false).map (r=> (r._1,r._2))

TweetIncrease.take(1).foreach(r=> println ("hashtagName: "+r._1+", countX: "+r._2._1+", countY: "+r._2._2))


// Required to exit the spark-shell
sys.exit(0)
