import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import spark.implicits._


// Define case classe for input data
case class Hashtag(tokenType: String, month: String, count: Int, hashtagName: String)
// Read the input data
val hashtags = spark.read.
  schema(Encoders.product[Hashtag].schema).
  option("delimiter", "\t").
  csv("hdfs:///user/ashhall1616/bdc_data/assignment/t2/twitter.tsv").
  as[Hashtag]

// Bonus task:
// TODO: *** Put your solution here ***

val df1 = hashtags.toDF("tokenType1","month1","count1","hashtagName1")
val df2 = hashtags.toDF("tokenType2","month2","count2","hashtagName2")

val result = df1.join(df2,$"hashtagName1" === $"hashtagName2" and ($"month1" === ($"month2" - 1) || $"month1" === ($"month2" - 89)))

val refined = result.withColumn("Diff",($"count2" - $"count1")).sort(desc("Diff")                                                                        ).select("month1","count1","month2","count2","hashtagName1")

refined.collect.take(1).foreach(r=> println("\nHashtag Name : "+r(4)+"\nCount of month "+r(0)+" : "+r(1)+"\nCount of month "+r(2)+" : "+r(3)+"\n"))


// Required to exit the spark-shell
sys.exit(0)
