// Load the input data and split each line into an array of strings
val vgdataLines = sc.textFile("hdfs:///user/ashhall1616/bdc_data/assignment/t1/vgsales.csv")
val vgdata = vgdataLines.map(_.split(";"))


// Task 1d:
// TODO: *** Put your solution here ***

val result = vgdata.map(r => (r(4))).map(r=>(r,1)).groupByKey().map(r => (r._1,r._2.sum))

val countRdd = vgdata.count()

val resultRdd = result.map(r => (r._1,r._2,(r._2*100/countRdd).toDouble)).sortBy(r=>r._3, false).take(50).foreach(println)


// Required to exit the spark-shell
sys.exit(0)
