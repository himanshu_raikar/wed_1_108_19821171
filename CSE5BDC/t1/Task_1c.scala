// Load the input data and split each line into an array of strings
//import org.apache.spark.rdd.RDD
val vgdataLines = sc.textFile("hdfs:///user/ashhall1616/bdc_data/assignment/t1/vgsales.csv")
val vgdata = vgdataLines.map(_.split(";"))

// Task 1c:
// TODO: *** Put your solution here ***

val result = vgdata.map(r => (r(3),(r(5).toDouble,r(6).toDouble,r(7).toDouble))).map(r => (r._1, (r._2._1+r._2._2+r._2._3))).reduceByKey(_+_)

result.take(3)

val max = result.sortBy(_._2, false).first()
val min = result.sortBy(_._2,true).first()
println("\nHighest selling Genre: "+ max._1 +" & Gross Sales (in Millions): "+max._2+"\nLowest selling Genre: "+ min._1 +" & Gross Sales (in Millions): "+min._2+"\n")

//Highest selling Genre: Action & Gross Sales (in Millions): 1562.6300000000103
//Lowest selling Genre: Strategy & Gross Sales (in Millions): 163.50000000000003

// Required to exit the spark-shell
sys.exit(0)