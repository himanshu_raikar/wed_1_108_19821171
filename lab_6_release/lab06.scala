// CSE3BDC/CSE5BDC Lab 06 - Spark DataFrames and Datasets
/*******************************************************************************
 * Exercise 1
 * Write a case class called Fruit which corresponds to the data in
 * Input_data/TaskA/fruit.csv
 */
// TODO: Write your code here
case class Fruit(name: String, price: Float, stock: Int)

import org.apache.spark.sql.Encoders
val fruitSchema = Encoders.product[Fruit].schema

val fruitDF = spark.read.option("header", "true").schema(fruitSchema).csv("file:///home/user198211713102/wed_1_108_19821171/lab_6_release/Input_data/TaskA/fruit.csv")

fruitDF.map(f => f.getAs[String]("name")).first
//String = apple

val fruitDS = fruitDF.as[Fruit]

fruitDS.map(f => f.name).first
//String = apple

/*******************************************************************************
 * Exercise 2
 * Convert fridgeParquetDF into a Dataset and show the first 10 rows.
 */
// TODO: Write your code here
val fridgeParquetDS = fridgeParquetDF.as[Fridge]
fridgeParquetDS.show(10)

// RESULT
+---------------+-----------+----------+-------------+
|          brand|    country|     model|   efficiency|
+---------------+-----------+----------+-------------+
|          KOXKA|      Spain|     M61M1|         11.1|
|          KOXKA|      Spain|     M62M1|         11.1|
|          KOXKA|      Spain|     M63M1|         11.1|
|HUSSMANN IMPACT|New Zealand|C2.2NXLE-M|13.6513844757|
|HUSSMANN IMPACT|New Zealand|  C2.2NXLE|10.7959605233|
|HUSSMANN IMPACT|New Zealand|      C6HE|13.3664104882|
|HUSSMANN IMPACT|New Zealand|      C6LE|13.3664104882|
|HUSSMANN IMPACT|New Zealand|      C6HE|11.5788878843|
|HUSSMANN IMPACT|New Zealand|      C6LE|11.5788878843|
|HUSSMANN IMPACT|New Zealand|       D5E| 9.7306346827|
+---------------+-----------+----------+-------------+
only showing top 10 rows


/*******************************************************************************
 * Exercise 3
 * Using DataFrames, find the full name of every country in Oceania (continent “OC”).
 * Show the first 10 country names in ascending alphabetical order.
 */
// TODO: Write your code here
val continentDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/continent.json")

val namesDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/names.json")

continentDF.filter($"continent" === "OC").
select($"countryCode").
join(namesDF,"countryCode").
select($"name").
orderBy($"name".asc).
show(10)

// RESULT

+----------------+
|            name|
+----------------+
|  American Samoa|
|       Australia|
|    Cook Islands|
|      East Timor|
|            Fiji|
|French Polynesia|
|            Guam|
|        Kiribati|
|Marshall Islands|
|      Micronesia|
+----------------+

/*******************************************************************************
 * Exercise 4
 * Calculate the average refrigerator efficiency for each brand. Order the results
 * in descending order of average efficiency and show the first 5 rows.
 */
// TODO: Write your code here

fridgeDF.groupBy($"brand").agg(avg("efficiency").as("AVG")).orderBy($"AVG".desc).show(5)

//RESULT

+--------------------+------------------+
|               brand|               AVG|
+--------------------+------------------+
|Minus Forty Techn...|     34.5190774644|
|             INFRICO|24.447894024066667|
|  Jono Refrigeration|     22.2967678746|
|              Imbera| 21.74823766813333|
|  TRUE MANUFACTURING|20.504535147400002|
+--------------------+------------------+

/*******************************************************************************
 * Exercise 5
 * Redo Exercise 3 using spark.sql instead of the DataFrames API.
 */
// TODO: Write your code here

namesDF.createOrReplaceTempView("names")
spark.sql(""" SELECT name FROM names INNER JOIN continents ON names.countryCode==continents.countryCode WHERE continent=='OC' ORDER BY name """).show(10)

// RESULT

+----------------+
|            name|
+----------------+
|  American Samoa|
|       Australia|
|    Cook Islands|
|      East Timor|
|            Fiji|
|French Polynesia|
|            Guam|
|        Kiribati|
|Marshall Islands|
|      Micronesia|
+----------------+


/*******************************************************************************
 * Exercise 6
 * Using toPercentageUdf, add a new column to fractionDF called “percentage”
 * containing the fraction as a formatted percentage string and drop the original
 * fraction column.
 */
// TODO: Write your code here
val percentageDF = fractionDF.drop($"fraction")withColumn("Percentage", toPercentageUdf($"count"/continentDF.count))
percentageDF.show()
// TODO: Copy and paste the result here

+---------+-----+----------+
|continent|count|Percentage|
+---------+-----+----------+
|       NA|   41|    16.40%|
|       SA|   14|     5.60%|
|       AS|   52|    20.80%|
|       AN|    5|     2.00%|
|       OC|   27|    10.80%|
|       EU|   53|    21.20%|
|       AF|   58|    23.20%|
+---------+-----+----------+


/*
 * SCRATCHPAD
 
case class Fridge(brand: String, country: String, model: String, efficiency: Double)

import org.apache.spark.sql.Encoders
val fridgeSchema = Encoders.product[Fridge].schema

val fridgeDF = spark.read.option("header", "true").schema(fridgeSchema).csv("/user/ashhall1616/bdc_data/lab_6/fridges.csv")
 
fridgeDF.map(f => f.getAs[String]("brand")).first

val fridgeDS = fridgeDF.as[Fridge]

fridgeDS.map(f => f.brand).first
---------------------------------------------------------------------------------
fridgeDF.printSchema
fridgeDS.printSchema
fridgeDF.show(5)
fridgeDS.show(5)
----------------------------------------------------------------------------------
fridgeDF.write.mode("overwrite").csv("lab6/task-b/fridges.csv")
fridgeDF.write.mode("overwrite").csv("file:////home/user198211713102/lab6/task-b/fridges.csv")

fridgeDF.write.mode("overwrite").parquet("lab6/task-b/fridges.parquet")
val fridgeParquetDF = spark.read.parquet("lab6/task-b/fridges.parquet")

fridgeParquetDF.printSchema
----------------------------------------------------------------------------------
val continentDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/continent.json")
val filteredDF = continentDF.filter($"continent" === "EU")
filteredDF.show(5)
val europeDF = filteredDF.select($"countryCode")
europeDF.show(5)

val currencyDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/currency.json")
val europeCurrDF = europeDF.join(currencyDF, "countryCode")
europeCurrDF.show(5)

val grouped = europeCurrDF.groupBy($"currency")
grouped.show(5) //doesnt work on RelationalGroupedDataset
val currCountDF = grouped.count()
currCountDF.show(5)

val currSortedDF = currCountDF.orderBy($"count".desc)
currSortedDF.show(5)
//EUR used in 25 countries and GBP is used in 4 countries
----------------------------------------------------------------------------------
val continentDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/continent.json")

val currencyDF = spark.read.json("/user/ashhall1616/bdc_data/lab_6/currency.json")

continentDF.filter($"continent" === "EU").
select($"countryCode").
join(currencyDF,"countryCode").
groupBy($"currency").
count().
orderBy($"count".desc).
show(5)
----------------------------------------------------------------------------------
continentDF.createOrReplaceTempView("continents")
val resultDF = spark.sql("SELECT DISTINCT continent from continents")
resultDF.show
----------------------------------------------------------------------------------
currencyDF.createOrReplaceTempView("currencies")
spark.sql("""SELECT currency, COUNT(1) AS count FROM continents INNER JOIN currencies ON continents.countryCode = currencies.countryCode WHERE continent = 'EU' GROUP BY currency ORDER BY count DESC""").show(5)

def function1() {
spark.sql("CELECT continent from continents").show(5)
}

def function2() {
continent.celect($"continent").show(5)
}
----------------------------------------------------------------------------------

val countDF = continentDF.groupBy($"continent").count()
val fractionDF = countDF.withColumn("fraction",$"count"/continentDF.count)
fractionDF.show(7)

----------------------------------------------------------------------------------

val countryPairDF = continentDF.as("df1").join(continentDF.as("df2"),$"df1.continent" === $"df2.continent" and $"df1.countryCode" < $"df2.countryCode")

countryPairDF.printSchema
countryPairDF.where($"continent" === "OC").show(5)
continentDF.show(1)

val df1 = continentDF.toDF("continent1","countryCode1")
val df2 = continentDF.toDF("continent2","countryCode2")
val countryPairDF = df1.join(df2, $"continent1" === $"continent2" and $"countryCode1" < $"countryCode2")

countryPairDF.where($"continent1" === "OC").show(5)

val selectedDF = countryPairDF.select($"continent1", $"countryCode1", $"countryCode2")

val droppedDF = countryPairDF.drop($"continent2")

selectedDF.show(5)
droppedDF.show(5)

----------------------------------------------------------------------------------

def toPercentage(x: Double): String = {
"%.2f%%".format(x * 100)
}

val toPercentageUdf = spark.udf.register[String, Double]("toPercentage", toPercentage)

fractionDF.select($"continent", toPercentageUdf($"fraction")).show()

fractionDF.createOrReplaceTempView("fractions")
spark.sql("SELECT continent, toPercentage(fraction) FROM fractions").show()

----------------------------------------------------------------------------------

 * Play around, and save any helpful Scala/Spark commands below this section
 */



