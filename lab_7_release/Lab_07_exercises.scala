/*
    TODO: [Exercise 1]
    Write the three different queries mentioned in step 7 and exercise 1.
    Below each query write the amount of data read in to complete that query, then
    answer the question at the bottom.
*/
// When the date is EQUAL to "2017-11": 807.0 B

// When the date is BEFORE "2017-11": 3.2 KB

// When the date is AFTER "2017-11": 809.0 B

// Is Spark able to optimise the reading partitioned data when the filtering is
// not using the equality operator?

//Answer : Yes it is able to optimise


/*
    TODO: [Exercise 2]
    After trying different values for the tree depth argument of the treeReduce
    function, compare the number of stages used in the Spark web interface and
    make a brief comment about your findings.
    
    Ans - The depth level 2 is similar to mentioning no depth. Spark will automatically treat depth level 2 if explicitly not mentioned. In the depth level 3 we see one more treereduce operation getting added and data shuffle of 1206.0 B is done for this extra stage.
*/




/*
    TODO: [Exercise 3]
    After changing the order of the filtering and sorting, compare the input
    and shuffle data sizes for the RDD query. Make a note of them below.
*/
// Sorting then filtering: Input Data-197.4 KB / Shuffle Data-19.9KB

// Filtering then Sorting: Input Data-197.4 KB / Shuffle Data-9.6KB


/*SCRATCHPAD

val numberRdd = sc.parallelize(List(1,5,4,1,6))
val sumRdd = numberRdd.map(x => (x%2,x)).reduceByKey(_+_)
sumRdd.collect

case class DailyWeather (date:String, mslp_9am: Double, mslp_3pm: Double)
import org.apache.spark.sql.Encoders
val weatherSchema = Encoders.product[DailyWeather].schema
val weatherDF = spark.read.option("header", "true").schema(weatherSchema).csv("/user/ashhall1616/bdc_data/lab_7/weather.csv")

val toYearMonthUdf = spark.udf.register[String, String]("toYearMonth", _.substring(0,7))
val weatherMonthDF = weatherDF.withColumn("yearMonth", toYearMonthUdf($"date"))
weatherMonthDF.filter($"yearMonth" === "2017-11").groupBy("yearMonth").avg("mslp_9am").show()

-----------------------------------------------------------------------------------

weatherMonthDF.write.mode("overwrite").partitionBy("yearMonth").parquet("task-b.parquet")

val weatherMonthDF = spark.read.parquet("task-b.parquet")
weatherMonthDF.filter($"yearMonth" === "2017-11").groupBy("yearMonth").avg("mslp_9am").show()

val weatherMonthDFAfter = spark.read.parquet("task-b.parquet")
weatherMonthDFAfter.filter($"yearMonth" > "2017-11").groupBy("yearMonth").avg("mslp_9am").show()

val weatherMonthDFBefore = spark.read.parquet("task-b.parquet")
weatherMonthDFBefore.filter($"yearMonth" < "2017-11").groupBy("yearMonth").avg("mslp_9am").show()

-----------------------------------------------------------------------------------

val weatherMonthDF = spark.read.parquet("task-b.parquet")
val minMaxDF = weatherMonthDF.groupBy($"yearMonth").agg(min($"mslp_3pm"), max($"mslp_3pm"))

minMaxDF.orderBy($"max(mslp_3pm)".desc).show(3)
minMaxDF.orderBy($"min(mslp_3pm)".asc).show(3)

minMaxDF.cache()
minMaxDF.orderBy($"max(mslp_3pm)".desc).show(3)
minMaxDF.orderBy($"min(mslp_3pm)".asc).show(3)

-----------------------------------------------------------------------------------

val rangeRdd =sc.parallelize(1 to 10000, 20)
rangeRdd.reduce(_+_)
rangeRdd.treeReduce(_+_)

rangeRdd.treeReduce(_ + _, 2)
rangeRdd.treeReduce(_ + _, 3)

-----------------------------------------------------------------------------------

val pokedexDF = spark.read.json("/user/ashhall1616/bdc_data/lab_7/pokedex.jsonl")
pokedexDF.cache.count()
val pokedexRDD = pokedexDF.rdd
pokedexRDD.cache.count()

-----------------------------------------------------------------------------------

pokedexRDD.sortBy(_.getAs[Double]("spawn_chance"), false).filter(_.getAs[Any]("next_evolution") == null).map(_.getAs[String]("name")).take(5)

pokedexDF.orderBy($"spawn_chance".desc).filter($"next_evolution".isNull).select($"name").take(5)

-----------------------------------------------------------------------------------

pokedexRDD.filter(_.getAs[Any]("next_evolution") == null).sortBy(_.getAs[Double]("spawn_chance"), false).map(_.getAs[String]("name")).take(5)

pokedexDF.filter($"next_evolution".isNull).orderBy($"spawn_chance".desc).select($"name").take(5)

-----------------------------------------------------------------------------------

pokedexRDD.unpersist()

val pokemonByType = pokedexRDD.map(r => (r.getAs[Seq[String]]("type").mkString("_"), r))

import org.apache.spark.sql.Row
def mostCommon(p1: Row, p2: Row): Row = {
if(p1.getAs[Double]("spawn_chance") > p2.getAs[Double]("spawn_chance")) {
return p1
} else {
return p2
}
}

val commonPokemonG = pokemonByType.groupByKey().map(_._2.reduce(mostCommon))
val commonPokemonR = pokemonByType.reduceByKey(mostCommon)
commonPokemonG.count
commonPokemonR.count

*/